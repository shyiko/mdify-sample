# Basics of Markdown syntax

## Line Breaks

End a line with two or more spaces to get a break:

    Sentence 1,  →
    sentence 2.
Sentence 1,  
sentence 2.

## Headers

    Header 1 →
    ========
Header 1
========

    Header 2 →
    --------
Header 2
--------

`# Header 1` →
# Header 1

`## Header 2` →
## Header 2

`### Header 3` →
### Header 3

`#### Header 4` →
#### Header 4

`##### Header 5` →
##### Header 5

`###### Header 6` →
###### Header 6

## Blockquotes

`> blockquote` →
> blockquote

## Lists

    1. First item  →
    2. Second item
1. First item
2. Second item

        * First item  →
        * Second item
        (* can be replaced with + or -)

    * First item
    * Second item

## Code Blocks

    `code` →
`code`

    <at least four spaces>code →

    code


    ```js
    function(message) { alert(message); } →
    ```
```js
function(message) { alert(message); }
```

## Horizontal Rules

    --- →
---

    *** →
***

## Links

`[url with title](http://url.com "title")` → [url with title](http://url.com "title")

`[url without title](http://url.com)` → [url without title](http://url.com)

`[url][url_id]` with `[url_id]: http://url "title"` somewhere in the document → [url][url_id]
[url_id]: http://url "title"

## Emphasis

`*italic*` → *italic*

`_italic_` → _italic_

`**bold**` → **bold**

`__bold__` → __bold__

## Images

` ![alt text](/path/img.png "title")` → ![alt text](favicon.ico "title")

`![alt text][img_id]` with `[img_id]: /url/to/img.jpg "title"` somewhere in the document → ![alt text][img_id]
[img_id]: favicon.ico "title"



